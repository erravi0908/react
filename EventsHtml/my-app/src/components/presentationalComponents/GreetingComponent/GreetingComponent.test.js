import React from "react";
import { shallow } from "enzyme";
import GreetingComponent from "./GreetingComponent";

describe("GreetingComponent", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<GreetingComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
