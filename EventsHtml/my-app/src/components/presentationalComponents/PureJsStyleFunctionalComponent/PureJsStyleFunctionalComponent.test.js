import React from "react";
import { shallow } from "enzyme";
import PureJsStyleFunctionalComponent from "./PureJsStyleFunctionalComponent";

describe("PureJsStyleFunctionalComponent", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<PureJsStyleFunctionalComponent />);
    expect(wrapper).toMatchSnapshot();
  });
});
