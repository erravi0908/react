import "./Player.css";

// passed state from Parent to child as Props
// Player Compoent is receivinng the state and settter as props in it.
var Player= ({removePlayer,players})=>{


  //calling the function to set the state as [] blank
  // here removePlayer is alias name of setPlayers of the parentConpoment which are passed as props
  // to the child Component
  function handleClick(s)
{
    removePlayer([])

}

return (
  <div>
      Player Child Component
      {/*  resuing the playersList li list, that is received as prop in the child component*/}
      {players}
       {/* Now writing logic to delete element from the list; i.e from state of the parent Component */}
       {/*added event handler on each span  */}
    <button onClick={()=>handleClick()}>{players}</button>


  </div>
)}

// 



export default Player;
