//import MyComponent from './components/myComponent';
import GreetingComponent from  './components/presentationalComponents/GreetingComponent';
import PureJsStyleFunctionalComponent from './components/presentationalComponents/PureJsStyleFunctionalComponent/PureJsStyleFunctionalComponent';
import {useState} from 'react';
import Player from './components/presentationalComponents/Player';
// app will act as parent component
// we will pass its state to child component and allow the child component to modify it also
// this we do by passing the setters method of parent state variable

function App() {

  // here players is the state variable of app component
  // setPlauers is setter method of app component
  
  // defining the state in the functional Component using useState() hook
  const[players,setPlayers]=useState([{"name":"ravi","age":10},{"name":"vinay","age":20}]);


  // presentation applied to the data
  const playersList=players.map((player)=>{
    return <li key={player.toString()}> Player : {player.name}  Age : {player.age}</li>
  })

  return (
    <div className="App">
    {/* calling child functional component by passing msg as props value*/}
      <GreetingComponent msg="JSX Style Functional Componet"/>
    
    
    {/* calling child functional component by passing msg as props value*/}
     <PureJsStyleFunctionalComponent msg="pureJsStyle FunctionalComponent" list={["a","b","c"]}/>
    
     <h1>Players List as li element</h1>
    
    
    {/* displaying the state of parent component named App.js 
        with help of rendering logic written using player.map() function.
    */}
    {playersList}


    {/* Now Passing the state of parent to other child as props, and allowing the child to modify it also
       by using setter of parent component, these setters are also passed as props to the child component 
    */}


    <br/><br/>
    <h3>To This child component we will pass state ot parent Componet and also passed
        setters of the state of parent component, so that the child can modify the parent state also.
        All these will be passed as prop the child;
        
    </h3>
    <Player  removePlayer={setPlayers} players={playersList}/>
    
    </div>
  );
}

export default App;
